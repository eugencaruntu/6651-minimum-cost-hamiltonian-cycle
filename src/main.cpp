/*
 * 6651 PA1
 * Eugen Caruntu, 29077103
 */

#include <iostream>
#include <fstream>
#include <vector>
#include <climits>

using namespace std;

// Produce all unique paths
vector<vector<int>> generate_unique_paths(int);

// Wrapper to generate all permutations
vector<vector<int>> generate_permutations(int);

// Make permutations (implemented to avoid usage of next_permutation from <algorithm>)
void permute(int, vector<bool>, vector<int>, vector<vector<int>> &);

// Helper function to reverse a vector (implemented to avoid usage of reverse from <algorithm>)
vector<int> reverse_vector(const vector<int> &);

// Helper function to print vectors (used for debugging)
void print_vector(const vector<int> &);

// Helper function to print vectors of vectors (used for debugging)
void print_vector_of_vectors(const vector<vector<int>> &);


/**
 * Find the minimum cost Hamiltonian cycle in a complete graph
 * @param argc the number of arguments
 * @param argv the vector of arguments
 * @return 0 if successful, -1 or exit code otherwise
 */
int main(int argc, char *argv[]) {

    if (argc != 3) {
        cout << "Error: incorrect number of arguments " << endl;
        return -1;
    }

    // The input and output streams provided as arguments
    fstream input(argv[1]);
    ofstream outfile(argv[2]);

    if (!input) {
        cout << "Invalid input file: " << argv[1] << endl;
        return -1;
    }

    if (!outfile) {
        cout << "Can't create output file: " << argv[2] << endl;
        return -1;
    }

    // The number of datasets
    int M;
    input >> M;

    // For each of the M datasets, create a matrix representation of the graph and populate it with the weights
    for (int dataset = 0; dataset < M; dataset++) {
        // The vertices and the edges
        int V, E;
        input >> V >> E;

        // An adjacency matrix is a more suitable representation for a complete graph since accessing paths is O(1) (space is similar to adjacency list for complete graphs)
        int graph[V][V];

        // The indices representing the vertices (u, v) and their weight (w)
        int u, v, w;

        // Reading the input for each edge and populate the weights in the matrix (we end up with a symetric matrix for the undirected graph)
        for (int e = 0; e < E; e++) {
            input >> u >> v >> w;
            graph[u][v] = w;
            graph[v][u] = w;
        }

        // add a new line before each dataset except first one (this avoids adding a line at the end of file)
        if (dataset != 0) {
            outfile << endl;
        }

        // Generate all unique possible paths
        vector<vector<int>> paths = generate_unique_paths(V);

        // Find the path with the minimum cost
        int min_cost = INT_MAX;

        // Calculate the cost for each path and determine the minimum
        for (const auto &p:paths) {
            int cost = 0;
            for (int i = 0; i < p.size() - 1; i++) {    // do not add the cost for last leg that closes the cycle
                cost += graph[p[i]][p[i + 1]];
            }
            min_cost = min(cost, min_cost);
        }

        // write to file
        outfile << min_cost;

        /* ===== Console output ===== */
/*
        // Render the matrix representation
        cout << "Matrix:" << endl;
        for (int row = 0; row < V; row++) {
            for (int col = 0; col < V; col++) {
                if (row == col) {
                    cout << 0 << '\t';
                } else {
                    cout << graph[row][col] << '\t';
                }
            }
            cout << endl;
        }

        // Render the vector of paths on console
        cout << "Permutations:" << endl;
        print_vector_of_vectors(paths);
        cout << endl;

*/
        /* ===== End of console output ===== */

    }

    // Close the input stream
    input.close();

    // Close the output stream
    outfile.close();

    return 0;
}

/**
 * Make a vector of paths and fill it with all possible paths obtained through permutations (the graphs are supposed to be complete).
 * Then we compare them with any reverse path and collect only 'unique paths'
 * @param V is the number of vertices in the graph
 * @return a vector of unique paths vectors
 */
vector<vector<int>> generate_unique_paths(int V) {
    vector<vector<int>> all_possible_paths = generate_permutations(V);
    vector<vector<int>> unique_paths;

    // Check for any paths that are the same once reversed. Collect only 'unique' paths.
    for (int i = 0; i < all_possible_paths.size(); i++) {
        bool unique = true;
        vector<int> reversed = reverse_vector(all_possible_paths[i]);
        for (int j = i + 1; j < all_possible_paths.size(); j++) {
            if (all_possible_paths[j] == reversed) {
                unique = false;
                break;
            }
        }
        if (unique) {
            all_possible_paths[i].push_back(
                    all_possible_paths[i][0]); // ad the first element on last position to have a complete path
            unique_paths.push_back(all_possible_paths[i]);
        }
    }

    return unique_paths;
}

/**
 * Wrapper function to return permutations of a vector of integers of size n.
 * @param n the size of the vector to be used
 * @return a vector of vectors with all all possible permutations
 */
vector<vector<int>> generate_permutations(int n) {
    vector<vector<int>> permutations;

    // populate the initial vector of integers from 0 to n
    vector<int> numbers(n);
    for (int i = 0; i < n; i++) {
        numbers[i] = i;
    }

    // prepare a boolean vector of false flags
    vector<bool> chosen_flags(numbers.size());

    // produce all possible permutations
    permute(0, chosen_flags, numbers, permutations);

    return permutations;
}

/**
 * Recursively generate all possible permutations for an initial vector by choosing one element at a time.
 * The use of a boolean vector of flags allows to read and set the positions as chosen or not.
 * For example, once the first element is chosen we recurse on the rest of the vector, and we keep track of what was used.
 * Elements get to be reused once a permutation is found (when last element is reached).
 * Each such permutation is pushed in permutation vector in the base case.
 *
 * This idea exploits the fact that the numbers are sorted,
 * therefore we can use the chosen vector position to set the values in the vector being permuted.
 * @refitem{https://www.youtube.com/watch?v=78t_yHuGg-0}
 *
 * @param current the position being permuted
 * @param chosen a boolean vector indicating if an element was already used
 * @param numbers the vector being used to generate the permutations
 * @param permutations is the vector of permutation vectors being collected (passed as reference)
 */
void permute(int current, vector<bool> chosen, vector<int> numbers, vector<vector<int>> &permutations) {
    unsigned long last = numbers.size();
    if (current == last) {
        permutations.push_back(numbers);    // once we have a permutation add it to the permutation vector
        return;
    }
    int i = 0;                      // start with first element
    while (i < last) {
        if (!chosen[i]) {           // if not chosen already,
            numbers[current] = i;   // change the current value of numbers vector to the chosen index number
            chosen[i] = true;       // mark the index in boolean vector as chosen so we dont reuse it in current permutation being produced
            permute(current + 1, chosen, numbers, permutations);    // do it again for the next position in the vector
            chosen[i] = false;      // change back the used index in boolean vector so we can used it later in other permutations
        }
        i++;
    }
}


/**
 * Reverse a vector of int by using a backward iterator to push the values into a new vector
 * @param vec the vector passed as reference
 * @return a reverted vector
 */
vector<int> reverse_vector(const vector<int> &vec) {
    vector<int> rev_vec(vec.size());
    for (auto it = vec.rbegin(); it != vec.rend(); ++it) {
        rev_vec.push_back(*it); // dereference the iterator to get the value
    }
    return rev_vec;
}

/**
 * Utility function to print a vector of vectors
 * @param vectors the vector containing the vectors
 */
void print_vector_of_vectors(const vector<vector<int>> &vectors) {
    for (auto &vector : vectors) {
        print_vector(vector);
    }
}

/**
 * Utility function to print a vector
 * @param vector the vector to be printed
 */
void print_vector(const vector<int> &vector) {
    for (auto &i : vector) {
        cout << i;
    }
    cout << endl;
}
